#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 19:02:20 2023

@author: belle
"""

import sys
import pandas as pd
import matplotlib.pyplot as plt
from PyQt5.QtWidgets import QApplication, QMainWindow, QListView, QVBoxLayout, QWidget, QLabel, QListWidget, QPushButton
from PyQt5.QtCore import Qt, QStringListModel
import pprint

pd.set_option('display.max_columns', None)
####Import Data into pandas ###################################################

points_df = pd.read_csv('plasticc_train_lightcurves.csv')

objects_metadata_df = pd.read_csv('plasticc_train_metadata.csv')

#merge the two dataframes using object_id so its easier to work with later
merged_df = pd.merge(points_df, objects_metadata_df, on='object_id')

# get list of all individ object_ids. should already be unique if you're pulling
# it from metadata as opposed to merged_df
valid_object_ids = objects_metadata_df['object_id'] #.unique()

###############################################################################

class DataFrameProcessor:
    def __init__(self):
        pass

    @staticmethod
    def grapher(dataframe):
        # Group the DataFrame by 'object_id'
        grouped_objects = dataframe.groupby('object_id')

        for object_id, object_group in grouped_objects:
            plt.figure(figsize=(10, 6))
            grouped_passbands = object_group.groupby('passband')
            for (passband, passband_group) in grouped_passbands:
                plt.errorbar(
                    passband_group['mjd'],
                    passband_group['flux'],
                    yerr=passband_group['flux_err'],  # Error values for the y-axis
                    label=f'Passband {passband}',
                    fmt='o'  # Format for the marker (optional)
                )
                # plt.scatter(passband_group['mjd'], passband_group['flux'], label=f'Passband {passband}')
            
            plt.xlabel('mjd')
            plt.ylabel('flux')
            plt.title(f'Object ID {object_id}')
            plt.legend()
            plt.xticks(rotation=45)
            plt.tight_layout()
            plt.show()
    
    # def print_dataframe_info(self, dataframe):
    #     # Print the information summary of the given dataframe
    #     print(dataframe['object_id'], dataframe.info())
        
    def print_dataframe_info(self, dataframes):
        print('type of datframes (should be list of multiple dfs: ', type(dataframes))
        # Loop through all selected DataFrames and print their information
        for dataframe in dataframes:
            print('type of dataframe singluar: ',type(dataframe))
            print(dataframe.info())
            print(dataframe.head())
            
    # def calculate_averages(self, dataframes):
    #     # Calculate the average of the 'measurement' column in each DataFrame
    #     averages = {}
    #     for object_id, dataframe in dataframes.items():
    #         average_measurement = dataframe['measurement'].mean()
    #         averages[object_id] = average_measurement

    #     print(averages)
        
    def get_average_measurement(self, dataframes):
        averages = {}
        for dataframe in dataframes:
            print('HELP', type(dataframe), dataframe)
            object_id = dataframe['object_id'].iloc[0]
            average_measurement = dataframe['flux'].mean()
            averages[object_id] = average_measurement

        print("\nAverage Measurement for Each Object:")
        pprint.pprint(averages)
        
class SummaryStats:
    def __init__(self):
        pass

    @staticmethod
    def calculate_average(dataframes):
        averages = {}
        for dataframe in dataframes:
            object_id = dataframe['object_id'].iloc[0]
            average_measurement = dataframe['flux'].mean()
            averages[object_id] = average_measurement

        # return averages
        print('\nAVERAGES', averages)
        pprint.pprint(averages)

    @staticmethod
    def calculate_standard_deviation(dataframes):
        std_deviations = {}
        for dataframe in dataframes:
            object_id = dataframe['object_id'].iloc[0]
            std_deviation = dataframe['flux'].std()
            std_deviations[object_id] = std_deviation

        # return std_deviations
        print('\nstandard deviation', std_deviations)
        pprint.pprint(std_deviations)
    
    
# =============================================================================
#     
#     def get_std_dev(self,dataframes):
#         std_devs = {}
#         for dataframe in dataframes:
#             object_id = dataframe['object_id'].iloc[0]
#             
# =============================================================================

    
    # def print_columns(self, dataframe):
    #     print(list(dataframe.columns))
        
    # maybe we want a function here that takes the dataframe for each object and puts it through an encoder?
    # things like word2vec have files that they have to train your transformer 
    # maybe we can figure out how to make one of those in this function    




# =============================================================================
# 
# # create grapher function that takes in a dataframe and prints out all specified objects
# 
# 
# def grapher(dataframe):
#     # Group the DataFrame by 'object_id'
#     grouped_objects = dataframe.groupby('object_id')
# 
#     for object_id, object_group in grouped_objects:
#         plt.figure(figsize=(10, 6))
#         grouped_passbands = object_group.groupby('passband')
#         for (passband, passband_group) in grouped_passbands:
#             plt.errorbar(
#                 passband_group['mjd'],
#                 passband_group['flux'],
#                 yerr=passband_group['flux_err'],  # Error values for the y-axis
#                 label=f'Passband {passband}',
#                 fmt='o'  # Format for the marker (optional)
#             )
#             # plt.scatter(passband_group['mjd'], passband_group['flux'], label=f'Passband {passband}')
#         
#         plt.xlabel('mjd')
#         plt.ylabel('flux')
#         plt.title(f'Object ID {object_id}')
#         plt.legend()
#         plt.xticks(rotation=45)
#         plt.tight_layout()
#         plt.show()
# =============================================================================

# here we make a GUI input thingy to select objects and passbands from
# yes i know i didn't have to but i really wanted to learn how to do that.
# this might make my life harder cause i'm not too sure why we're creating a class
# and how we decided what would go into it vs getting defined outside of the class

class GUI_filter(QMainWindow):
    def __init__(self, merged_df, metadata_df):
        super().__init__()
        self.processor = DataFrameProcessor()
        self.summary_stats = SummaryStats()
        self.dataframes_dict = self.create_dataframes_dict(merged_df)
        self.metadata_df = metadata_df
        self.initUI()
        
    #initUI defines what we're going to have in our GUI window
    def initUI(self):
        self.setWindowTitle('GUI Filter')
        self.setGeometry(100, 100, 400, 200)

        # Create a QLabel to display selected options
        self.selected_label = QLabel(self)
        self.selected_label.setGeometry(20, 150, 360, 30)

        # Get the object_id column from metadata_df and convert it to a list
        valid_object_ids = self.metadata_df['object_id'].tolist()

        # Create a QListView (List of options) for object_ids
        self.list_view_object_ids = QListView(self)
        self.list_view_object_ids.setSelectionMode(QListView.ExtendedSelection)
        self.list_view_object_ids.setGeometry(20, 20, 150, 100)

        # Create a QStringListModel for the object_id options
        self.list_model_object_ids = QStringListModel()
        self.list_model_object_ids.setStringList([str(obj_id) for obj_id in valid_object_ids])
        self.list_view_object_ids.setModel(self.list_model_object_ids)

        # Create a QListWidget (List of options) for color selection
        self.list_widget_passband = QListWidget(self)
        self.list_widget_passband.setSelectionMode(QListWidget.MultiSelection)
        self.list_widget_passband.setGeometry(200, 20, 150, 100)
        self.list_widget_passband.addItems(['0', '1', '2', '3', '4'])

        # Create an "Okay" button
        self.okay_button = QPushButton('Okay', self)
        self.okay_button.setGeometry(100, 130, 100, 30)
        self.okay_button.clicked.connect(self.on_okay_button_clicked)

        # Show the main window
        self.show()
    # create_dataframes_dict takes in the merged_df, makes an empty dictionary
    # then defines key value pairs where the key is obj_id, and the values are 
    # the entries from merged_df corresponding to the matching object_ids
    # and returns the dictionary to be used later!
    def create_dataframes_dict(self, merged_df):
        # Group merged_df by 'object_id' and create DataFrames for each group
        dataframes_dict = {}
        for obj_id, group in merged_df.groupby('object_id'):
            dataframes_dict[obj_id] = group

        return dataframes_dict
    # on_okay_button_click registers which object_ids and which passbands you've selected,
    # showing it on the GUI window
    # and applies the grapher function to the filtered dataframe
    def on_okay_button_clicked(self):
        # Get the selected object_ids from the QListView
        selected_indexes_object_ids = self.list_view_object_ids.selectionModel().selectedIndexes()
        selected_object_ids = [int(index.data(Qt.DisplayRole)) for index in selected_indexes_object_ids]

        # Get the selected colors from the QListWidget
        selected_indexes_colors = self.list_widget_passband.selectedIndexes()
        selected_colors = [int(index.data(Qt.DisplayRole)) for index in selected_indexes_colors]

        # Filter the DataFrame based on the selected object_ids and colors
        selected_dataframes = [self.dataframes_dict[object_id] for object_id in selected_object_ids]
        filtered_dataframe = pd.concat(selected_dataframes)
        filtered_dataframe = filtered_dataframe[filtered_dataframe['passband'].isin(selected_colors)]



        # Call the grapher method from the existing instance of DataFrameProcessor with the filtered DataFrame
        self.processor.grapher(filtered_dataframe)
        
        # Call the other methods from the existing instance of DataFrameProcessor with the list of selected dataframes
        self.processor.print_dataframe_info([filtered_dataframe])
        self.processor.get_average_measurement([filtered_dataframe])
        
        self.summary_stats.calculate_average([filtered_dataframe])
        self.summary_stats.calculate_standard_deviation([filtered_dataframe])


# =============================================================================
#         # Call the grapher function with the filtered DataFrame
#         # grapher(filtered_dataframe)
#         
#         # Call the grapher method from the DataFrameProcessor class with the filtered DataFrame
#         processor = DataFrameProcessor()
#         #grapher takes in one dataframe to create different plots i'm  fairly sure of this
#         processor.grapher(filtered_dataframe)
#         #print_dataframe_info needs to print all of them in one go so it takes in the collection of all of them
#         processor.print_dataframe_info(selected_dataframes)
#         # processor.calculate_averages(selected_dataframes)
#         print('type of selected_dfs: ') 
#         # Calculate and print the average measurement for each selected DataFrame
#         # averages = 
#         processor.get_average_measurement(selected_dataframes)
#         # pprint.pprint("\nAverage Measurement for Each Object:")
#         # for obj_id, avg in averages.items():
#         #     print(f"Object ID {obj_id}: {avg}")
#         
#         # processor.print_columns(filtered_dataframe)
#         
#         # processor.get_columns(filtered_dataframe)
# =============================================================================
        
        # Call other functions from the DataFrameProcessor class with the filtered DataFrame
# =============================================================================
#         processor.process_data(filtered_dataframe)
#         total_sum = processor.calculate_sum(filtered_dataframe, selected_column)
# =============================================================================
        # Call the DataFrameProcessor method with the filtered DataFrame
        # self.processor.get_average_measurement(filtered_dataframe)



        # Update the QLabel to display the selected options
        selected_colors_str = ', '.join(map(str, selected_colors))
        self.selected_label.setText(f"Selected Options: Object IDs - {', '.join(map(str, selected_object_ids))}, Colors - {selected_colors_str}")


# =============================================================================
# When a Python script is executed, Python sets the special variable __name__ to
# the string '__main__' if the script is being run as the main program.
# If the script is being imported as a module into another script, __name__ will
# be set to the name of the module instead.
# 
# The if __name__ == '__main__': block is used to specify code that should only
# be executed when the script is run directly, not when it is imported as a module.
# It ensures that certain code is only executed if the script is run from the
# command line or an IDE, and not when it is used as part of another program.
# 
# Using the if __name__ == '__main__': block is a good practice in Python because
# it allows you to create reusable code that can be imported and used in other
# scripts as modules without unintended side effects. It helps separate code meant
# for execution on its own from code that is intended for reuse and modularization.
#
#                                                                      -chatgpt
# =============================================================================

# am i sure I want it to be specific to this file?


if __name__ == '__main__':
    # merged_df = merged_df
    # metadata_df = objects_metadata_df
    app = QApplication(sys.argv)
    window = GUI_filter(merged_df, objects_metadata_df)
    sys.exit(app.exec_())

# total_sum = merged_df['measurement'].sum()

# print("Total Sum of Measurement Values:", total_sum)