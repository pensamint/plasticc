#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 16:03:56 2023

@author: belle
"""

####Import Libraries###########################################################
# what do we need
import numpy as np
import pandas as pd
import torch

import matplotlib.pyplot as plt

import itertools

# import tensorflow as tf

# make results deterministic
torch.backends.cudnn.enabled = False
random_seed = 1
torch.manual_seed(random_seed)

####Import Data into pandas ###################################################
# importing our data into pandas dataframes
points_df = pd.read_csv('plasticc_train_lightcurves.csv')

# points_df.head()

objects_metadata_df = pd.read_csv('plasticc_train_metadata.csv')

# objects_metadata_df.head()

####Merge Datasets, create plot devices########################################
# merge our dataframes into one so we can use object_ID and know everything
# prepare any lists as plot devices

merged_df = pd.merge(points_df, objects_metadata_df, on='object_id')

passbands = merged_df['passband'].unique()


####what do you wantto pull out?###############################################
# List of specific objects_id's you want to pull
# Get unique passbands from the DataFrame, and choose which ones you want to pull


selected_objects = [615, 713, 130779836]  #lists of ints

selected_passbands = [1, 2]

# print(type())

# =============================================================================
# 
# print(len(objects_metadata_df['object_id'].unique()))
# # 7848 unique IDs
# =============================================================================


####Create a grid##############################################################
# make a list of all instances you want to create

combinations = [[c1, selected_passbands] for c1 in selected_objects]

# print(type(combinations[0]))

# print(combinations)
# print(combinations[0][1][0])
# =============================================================================
# for i in combinations:
#     print(i)
# =============================================================================


# selected_grid = [(x, y) for x in selected_objects for y in selected_passbands]
# 
# grid = list(itertools.product(selected_objects, selected_passbands)) #makes combinations but doens't matter if you want more than 1 passband

# print(grid[0])

####create filtering function##################################################

#  OBJECT ID FILTER FUNCTION

dataframes_dict = {}


# print(type(dataframes))


#following func gets pandas df for selected ids

def filter_objects_by_id(merged_df, combinations):

    for i in combinations:
        # print(i)

    # Step 2: Filter the merged dataframe to select specific objects under specific passbands
        filtered_df = merged_df.loc[
            (merged_df['object_id'].isin(i)) #& combinations0 always prints the 0th 615. how do i iterate?
          #   (merged_df['passband'].isin(selected_passbands))
          ]
    
        # dataframes.update(filtered_df)
    # dataframes_dict['my_dataframe'] = filtered_df #an indent here doesn't seem to make a diff
    # 
        # Step 2: Generate a unique key for each DataFrame (e.g., using 'df_i')
        key = f'filtered_df_{i}'
        
        # Step 3: Add the DataFrame to the dictionary with the unique key
        dataframes_dict[key] = filtered_df
        
    

filter_objects_by_id(merged_df, combinations)
# 






# lets test the dictionary!
# must ask how to print NOTpop that means smtg else!
# print('dict?', dataframes_dict)


for key, value in dataframes_dict.items():
    print('screaming', key, ', crying, breaking things', value)
    print('does this work?', value['passband'])

