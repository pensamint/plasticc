#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 21:59:13 2023

@author: belle
"""
####Import Libraries###########################################################
# what do we need
import numpy as np
import pandas as pd
import torch

import matplotlib.pyplot as plt

import itertools

# import tensorflow as tf

# make results deterministic
torch.backends.cudnn.enabled = False
random_seed = 1
torch.manual_seed(random_seed)

####Import Data into pandas ###################################################
# importing our data into pandas dataframes
points_df = pd.read_csv('plasticc_train_lightcurves.csv')

# points_df.head()

objects_metadata_df = pd.read_csv('plasticc_train_metadata.csv')

# objects_metadata_df.head()

####Merge Datasets, create plot devices########################################
# merge our dataframes into one so we can use object_ID and know everything
# prepare any lists as plot devices

merged_df = pd.merge(points_df, objects_metadata_df, on='object_id')

passbands = merged_df['passband'].unique()


unique_object_ids = merged_df['object_id'].unique()

selection = unique_object_ids[:5]
print(selection)
print(type(unique_object_ids))

####what do you wantto pull out?###############################################
# List of specific objects_id's you want to pull
# Get unique passbands from the DataFrame, and choose which ones you want to pull


# selected_objects = [615, 713, 130779836]  #lists of ints

selected_objects = selection.tolist()
selected_passbands = [1, 2]

print(type(selected_objects))

# =============================================================================
# 
# print(len(objects_metadata_df['object_id'].unique()))
# # 7848 unique IDs
# =============================================================================


####Create a grid##############################################################
# make a list of all instances you want to create

combinations = [[c1, selected_passbands] for c1 in selected_objects]

# print(type(combinations[0]))

# print(combinations)
# print(combinations[0][1][0])
# =============================================================================
# for i in combinations:
#     print(i)
# =============================================================================


# selected_grid = [(x, y) for x in selected_objects for y in selected_passbands]
# 
# grid = list(itertools.product(selected_objects, selected_passbands)) #makes combinations but doens't matter if you want more than 1 passband

# print(grid[0])

####create filtering function##################################################


# =============================================================================
#  OBJECT ID FILTER FUNCTION

# dataframes_dict = {}


# # print(type(dataframes))


# #following func gets pandas df for selected ids

# def filter_objects_by_id(merged_df, combinations):

#     for i in combinations:
#         print(i)

#     # Step 2: Filter the merged dataframe to select specific objects under specific passbands
#         filtered_df = merged_df.loc[
#             (merged_df['object_id'].isin(i)) #& combinations0 always prints the 0th 615. how do i iterate?
#           #   (merged_df['passband'].isin(selected_passbands))
#           ]
    
#         # dataframes.update(filtered_df)
#     # dataframes_dict['my_dataframe'] = filtered_df #an indent here doesn't seem to make a diff
#     # 
#         # Step 2: Generate a unique key for each DataFrame (e.g., using 'df_i')
#         key = f'filtered_df_{i}'
        
#         # Step 3: Add the DataFrame to the dictionary with the unique key
#         dataframes_dict[key] = filtered_df
        
    

# filter_objects_by_id(merged_df, combinations)
# # 
# =============================================================================





# lets test the dictionary!
# must ask how to print NOTpop that means smtg else!
# print('dict?', dataframes_dict)



# df = pd.DataFrame(combinations) #wouldn't it be cool if we could have the grid as a pandas df? unless..

# print(type(df))


# =============================================================================
# 
# # PASSBAND FILTER FUNCTION
# maybe also prints what would be the two dict entries of two different objects 
# ...IF IT FILTERED THAT >:P
# 
# dataframes_dict = {}
# 
# 
# # print(type(dataframes))
# 
# 
# #following func gets pandas df for selected passbands
# 
# def filter_objects_by_passbands(merged_df, combinations):
# 
#     for i in combinations:
#         print('hi',i)
#         for j in i:
#             print('merp',j)
#             if type(j) == list:
#                 print('LIST?', j)
#                 for k in j:
#                     
#                 
# 
#             # Step 2: Filter the merged dataframe to select specific objects under specific passbands
#                     filtered_df = merged_df.loc[
#             
#                         # (merged_df['object_id'].isin(i)) & #combinations0 always prints the 0th 615. how do i iterate?
#                         (merged_df['passband'].isin(j))
#                      ]
#     
#         # dataframes.update(filtered_df)
#     # dataframes_dict['my_dataframe'] = filtered_df #an indent here doesn't seem to make a diff
#     # 
#                 # Step 2: Generate a unique key for each DataFrame (e.g., using 'df_i')
#                 key = f'filtered_df_{i}'
#                 
#                 # Step 3: Add the DataFrame to the dictionary with the unique key
#                 dataframes_dict[key] = filtered_df
#         
#     
# 
# filter_objects_by_passbands(merged_df, combinations)
# 
# 
# # lets test the dictionary!
# # must ask how to print NOTpop that means smtg else!
# print('dict????', dataframes_dict)
# 
# 
# 
# 
# =============================================================================

# print(type(dataframes))





# =============================================================================
# FRANKENSTIEN!
# =============================================================================
        
dataframes_dict = {}


# print(type(dataframes))



#following func gets pandas df for selected ids

def filter_fun(merged_df, combinations):

    for i in combinations:
        # print(i)
        
        # 
    # Step 2: Filter the merged dataframe to select specific objects under specific passbands
            
        ob_filtered_df = merged_df.loc[
            (merged_df['object_id'].isin(i)) #& combinations0 always prints the 0th 615. how do i iterate?
          #   (merged_df['passband'].isin(selected_passbands))
          ]
    
        # dataframes.update(filtered_df)
        # dataframes_dict['my_dataframe'] = filtered_df #an indent here doesn't seem to make a diff
        # Step 2: Generate a unique key for each DataFrame (e.g., using 'df_i')
        key = f'filtered_df_{i}'
        
        # Step 3: Add the DataFrame to the dictionary with the unique key
        dataframes_dict[key] = ob_filtered_df
        
        for j in i:
            # print('merp',j)
            if type(j) == list:
                # print('LIST?', j)
                # for k in j:
                #     print(k)
                

            # Step 2: Filter the merged dataframe to select specific objects under specific passbands
                pb_filtered_df = ob_filtered_df.loc[
        
                    # (merged_df['object_id'].isin(i)) & #combinations0 always prints the 0th 615. how do i iterate?
                    (ob_filtered_df['passband'].isin(j))
                  ]
                key = f'filtered_df_{i}'
                
                # Step 3: Add the DataFrame to the dictionary with the unique key
                dataframes_dict[key] = pb_filtered_df


        
        
    

filter_fun(merged_df, combinations)



# lets test the dictionary!
# must ask how to print NOTpop that means smtg else!
print('dict?', dataframes_dict)#['filtered_df_[615, [1, 2]]'])
print(list(dataframes_dict.keys()))






# print(type(dataframes))

# print(dataframes.items())

# df = pd.DataFrame.from_dict(dataframes)

# df.head(400)



# =============================================================================
# 
# def filtering(grid, dataframe):
#     
#     dataframe_collection = {}
#     
#     # dataframes = {
#     #     'DataFrame1': df1,
#     #     'DataFrame2': df2,
#     #     'DataFrame3': df3
#     # }
# 
#     
#     #i want this function to generate dataframes filtered by object_is and passbands
#     
#     for i in grid:
#         # print(i)
#         one_selected_object_list = [i[0]]
#         one_selected_passband_list = [i[1]]
#         new_dataframe = dataframe.loc[
#                 (dataframe['object_id'].isin(one_selected_object_list)) &
#                 (dataframe['passband'].isin(one_selected_passband_list))
#                 ]
#         # return(new_dataframe)
#         key = f'Round_{i}'
#     
#         value = i ** 2
# 
#         # Step 3: Add the key-value pair to the dictionary
#         aggregated_data[key] = value
# 
# filtering(selected_grid, merged_df)
# 
# 
# 
# 
# =============================================================================


# print()




# for key, value in dataframes_dict:
#     print(value[0])

        
# first_df = dataframes_dict['filtered_df_[615, [1, 2]]']


# print('test', type(first_df))

# print(first_df['flux'])

# column_to_print = 'flux'

# print("Column '{}' from the first DataFrame:".format(column_to_print))
    # Plotting the time series data using Matplotlib.
# plt.figure(figsize=(10, 6))
# plt.plot(first_df['mjd'], first_df['flux'])
# plt.xlabel('mjd')
# plt.ylabel('flux')
# plt.title('Time Series Data for filtered_df_[615, [1, 2]] in passband 1 and 2')
# plt.grid(True)
# plt.show()


for df in dataframes_dict.values():
    print(df.info())
    x = df['mjd']
    y = df['flux']
    errors = df['flux_err']
    plt.figure(figsize=(10, 6))
    
    plt.errorbar(x, y, yerr=errors, fmt='o', capsize=5)
    plt.xlabel('mjd')
    plt.ylabel('flux')
    plt.title(f'Time Series Data for {dataframes_dict.keys()}')
    plt.grid(True)
    plt.show()

# I want to create a grid of the object IDs and the passbands I'm selecting for


# for i in results:
#     plt.rcParams["figure.figsize"] = [7.50, 3.50]
#     plt.rcParams["figure.autolayout"] = True
#     x = np.linspace(-2, 2, 10)
#     y1 = np.sin(x)
#     y2 = np.cos(x)
#     plt.subplot(211)
#     plt.plot(y1)
#     plt.subplot(212)
#     plt.plot(y2)
#     plt.show()


